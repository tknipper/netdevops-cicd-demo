import sys
import re


def main():
    passwords = get_passwords("group_vars/all.yml")
    bad_pass = False
    for password in passwords:
        password_results = password_check(password)
        if not password_results['password_ok']:
            print(f"SNMP string security check failed for string {password}")
            bad_pass = True
    # If results come back false, one of the test have failed.  Report the error
    if bad_pass:
        print("SNMP string security check FAILED. Ensure strings match security standard before resubmitting job.")
        sys.exit(1)
    else:
        print("SNMP string security check PASSED.")
        sys.exit(0)


def password_check(password):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        8 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    """

    # calculating the length
    length_error = len(password) < 8

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None

    # overall result
    password_ok = not (length_error or digit_error or uppercase_error or lowercase_error or symbol_error)

    return {
        'password_ok': password_ok,
        'length_error': length_error,
        'digit_error': digit_error,
        'uppercase_error': uppercase_error,
        'lowercase_error': lowercase_error,
        'symbol_error': symbol_error,
    }


def get_passwords(filename):
    # Look for global variable file
    # Blank list for tracking found passwords
    snmp_strings = []
    with open(filename, 'r') as variable_file:
        lines = variable_file.readlines()
        for row in lines:
            # check if SNMP string is present
            word = 'snmp_password'
            # Using the find method, search for the value.  -1 will be returned if not found
            if row.find(word) != -1:
                # Split the string to grab the password value
                split_string = row.split(":")[1].strip()
                snmp_strings.append(split_string)
    return snmp_strings


main()



