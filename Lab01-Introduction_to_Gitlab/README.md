# Lab 01 - Introduction to Gitlab

This lab assumes you are already connected into the lab environment via VPN.  If you have not 
already connected, refer to the [README.md](../README.md) file in the root directory.

**Objective:** The purpose of this lab is to familiarize yourself with Gitlab interface and run a basic
CI/CD pipeline.  We will create a new repository, create CICD pipline file, run the pipeline file, and then import 
a new repository to start Lab 2 with.

## Table of Contents

[[_TOC_]]

## Step 1 - Login to Gitlab Portal and associated services
General details will be provided below:

| System  | IP Address  | Username  | Password  | Notes  |
| :--- | :--- | :--- | :--- | :--- |
| Gitlab Server | http://e16ubuntu1gitlab-ce.datalinklabs.local  | podXX where XX is your POD# (e.g. pod01, pod10) | Tr@nsform2023!  |   |
| Gitlab-runner VM  | SSH to assigned IP | ubuntu  | Tr@nsform2023!  |   |
| Cisco IOS devices  | Various | cisco  | cisco  |   |

| POD  | Cisco IOS Device  | Runner VM |      |  POD  |  Cisco IOS Device  | Runner VM | 
|---|---|---|---|---|---|---|
|  **POD01**  | 10.216.107.101  | 10.207.63.201 |  -  |  **POD21**  | 10.216.107.121  | 10.207.63.221 |
|  **POD02**  | 10.216.107.102  | 10.207.63.202 |  -  |  **POD22**  | 10.216.107.122  | 10.207.63.222 |
|  **POD03**  | 10.216.107.103  | 10.207.63.203 |  -  |  **POD23**  | 10.216.107.123  | 10.207.63.223 |
|  **POD04**  | 10.216.107.104  | 10.207.63.204 |  -  |  **POD24**  | 10.216.107.124  | 10.207.63.224 |
|  **POD05**  | 10.216.107.105  | 10.207.63.205 |  -  |  **POD25**  | 10.216.107.125  | 10.207.63.225 |
|  **POD06**  | 10.216.107.106  | 10.207.63.206 |  -  |  **POD26**  | 10.216.107.126  | 10.207.63.226 |
|  **POD07**  | 10.216.107.107  | 10.207.63.207 |  -  |  **POD27**  | 10.216.107.127  | 10.207.63.227 |
|  **POD08**  | 10.216.107.108  | 10.207.63.208 |  -  |  **POD28**  | 10.216.107.128  | 10.207.63.228 |
|  **POD09**  | 10.216.107.109  | 10.207.63.209 |  -  |  **POD29**  | 10.216.107.129  | 10.207.63.229 |
|  **POD10**  | 10.216.107.110  | 10.207.63.210 |  -  |  **POD30**  | 10.216.107.130  | 10.207.63.230 |
|  **POD11**  | 10.216.107.111  | 10.207.63.211 |  -  |  **POD31**  | 10.216.107.131  | 10.207.63.231 |
|  **POD12**  | 10.216.107.112  | 10.207.63.212 |  -  |  **POD32**  | 10.216.107.132  | 10.207.63.232 |
|  **POD13**  | 10.216.107.113  | 10.207.63.213 |  -  |  **POD33**  | 10.216.107.133  | 10.207.63.233 |
|  **POD14**  | 10.216.107.114  | 10.207.63.214 |  -  |  **POD34**  | 10.216.107.134  | 10.207.63.234 |
|  **POD15**  | 10.216.107.115  | 10.207.63.215 |  -  |  **POD35**  | 10.216.107.135  | 10.207.63.235 |
|  **POD16**  | 10.216.107.116  | 10.207.63.216 |  -  |  **POD36**  | 10.216.107.136  | 10.207.63.236 |
|  **POD17**  | 10.216.107.117  | 10.207.63.217 |  -  |  **POD37**  | 10.216.107.137  | 10.207.63.237 |
|  **POD18**  | 10.216.107.118  | 10.207.63.218 |  -  |  **POD38**  | 10.216.107.138  | 10.207.63.238 |
|  **POD19**  | 10.216.107.119  | 10.207.63.219 |  -  |  **POD39**  | 10.216.107.139  | 10.207.63.239 |
|  **POD20**  | 10.216.107.120  | 10.207.63.220 |  -  |  **POD40**  | 10.216.107.140  | 10.207.63.240 |

Using your local web browser, go to the Gitlab server URL http://e16ubuntu1gitlab-ce.datalink.local and login using the 
credentials and POD assignment above.  

Verify you can access via your SSH client (command line, Putty, SecureCRT) both the Cisco IOS device and the Gitlab-Runner VM assigned to your POD.  Please let an instructor 
know if you are unable to access any devices at this step. 

*Note:* You may see an error when SSH'ing into your router about invalid key exchanges and cyphers. If you do, login to 
to your router with the following syntax (with the proper IP address):

```
ssh -oKexAlgorithms=+diffie-hellman-group14-sha1 cisco@10.216.107.1XX
```

[Back to Top](#table-of-contents)

## Step 2 - Create your first repository

When you login to your POD account, the repository will be blank.  We will start by creating a new project (repository). 
Click __Create a project__ on the main screen and then choose __Create blank project__.

![Lab 01 - Create a Project](../images/Lab01-Create_a_project.jpg)    

The name is arbitrary but let's name the project **My First CICD Project**.  Notice that a URL with a slug gets created.  
Change the visibility level to Public just in case we need to troubleshoot.  Leave the Project configuration set to 
'Initialize repository with README' and click **Create project**.

![Lab 01 - Create blank project](../images/Lab01-Create_blank_project.jpg)    

[Back to Top](#table-of-contents)

## Step 3 - Explore the interface

Now that we have a project created we can start seeing some more details in Gitlab.  You can disable any warning that 
may appear as this is a lab instance.

![Lab 01 - My First CICD Project](../images/Lab01-My_first_cicd_project.jpg)    

### Main Page (Repository)
On the main page of the project repository screen there is a dropdown that says ```main↓```.  This indicates what branch of the version control system (VCS) 
repository we are working in. The plus sign to the right will allow you to create new files, folders, 
upload new files, create new branches or tags.  The blue button on the right that says **Clone** will give us 
links to clone the project to another repository.  At the top of the screen, the three dashes, will take you to the top level 
menu which has all projects, groups, and settings.  In this repository, the only file that has been created is the README.md file.  

The screen capture below shows the common items on the interface that we will be working with.  We will walk through each 
of these screens in the next few steps.

![Lab01 - Repository Overview](../images/Lab01-Repository_Overview.jpg)

### Integrated IDE & changes

The integrated IDE is a quick way to make changes directly in the repository.  You typically would use a separate IDE 
(VSCode, Pycharm, Sublime) to do development but for the purposes of this lab we will use the integrated IDE.  The IDE 
will integrate into the Gitlab change flow which will make it easy to do Issues/Merge requests.  The flowchart 
below shows a typical flow for changes in a version control system.  Let's follow this process to make our first change to the 
repository!

<div class="center">

```mermaid
graph TD
    A[Open in IDE] --> B[Edit Document]
    B --> C[Commit Changes to new branch]
    C --> D[Start Merge Request]
    D --> E[Create Merge Request]
    E --> F[Approve]
    F --> |*Delete Source Branch*| G[Merge]
```
</div>

Click on the README.md file and then click on the blue button labeled **Open in Web IDE**.  This will open a new tab 
with a text editor.  Notice that the text is the same as the previous page but it is now formatted and has 'markup' 
applied to it.  Let's make a change to the file and see how it is saved. It doesn't matter what you type but let's type 
the following right under the first heading that says 'My First CICD Project'. 

***Copy Button***

When copying data from this lab guide into the IDE it is VERY important that you use the copy button as shown in the image below.  
Using this button ensures that the proper formatting of the text is applied and makes sure you get all of the text in one move.  
Failure to do so at later points in the lab will give you failed YAML validation or other issues.

![Lab01 - Copy Button](../images/Lab01-Copy_Button.jpg)

***Edit README.md***

Add the following line to your README.md file at the top.
```
This is my first commit for a Gitlab repository.
```



As soon as you make a change, the **Create commit...** button will turn blue.  Before we click it, click at the top of 
the window where it says **Preview Markdown**.  Notice that it has added our text to the first section of the document.
  The changes will not be saved until the next step.

***Commit changes***

Go ahead and click **Create commit..**.  

Preview the changes.  The left side of the screen shows the original file, the right side shows our changes.  
Diffs are shown with RED highlights being removals and GREEN are additions.  

Choose the radio button **Create a new branch**.  Notice that a branch name gets created automatically. 
 Add a comment such as 'Updated README.md file' in the message box.  Make sure the check 
 box for starting a new merge request is checked and choose **Commit**.

***Add notes and merge request***

Since we created a merge request automatically we are brought directly to that page.  Notice the name of the branches 
at the top of the request.  We should be merging from a random branch name such as ```PODX-main-patch-12412``` into ```main```.  

Notice at the bottom of the screen you can look at commits and changes.  This will show you what was added or deleted 
with this merge request.  A merge request can contain multiple commits.  Each change submitted would be one commit. A file 
can change multiple times between merges and multiple files can be changed in the same merge.

Add some text to the description and make sure that 'delete a source branch' is still clicked, and 
choose the blue button **Create merge request**.

```
My First Gitlab Merge!!!!
```

***Approve changes & Merge***

Now we are at the approval stage.  This is where in production we may have approval or reviewers to validate our work 
before pushing to a production network.  You can once again view any changes or commits using some of the links at the 
top of the screen.

![Lab01 - Changes and Commits](../images/Lab01-My_First_Merge.jpg)

Go back to the Overview tab.  There are no approvers except you in the lab, so go ahead and click Approve (optional) and then 
click Merge.  Notice the 'delete source branch' box is ticked.  This will delete the no longer needed branch we were 
working in.    

***Verify Changes***

Verify that the changes were merged into the main branch.  Click on the repository icon in the left column.  It should 
now show an updated README.md file from the last few minutes and the new text added should appear in the screen below.

![Lab01 - Repository -> Files](../images/Lab01-Repository_Files.jpg)

Congrats, you have completed your first merge request in Gitlab!  Let's keep reviewing the interface details we will be 
using in the lab today.

[Back to Top](#table-of-contents)

### Issues (List & Boards)
Issues for the project should be documented under the Issue list.  Any issue such as bug fix request, feature request, or similar can be added to the list.  
Issues allow us to track the workflow of the many different flows of changes in our projects.  These issues can then be picked up by yourself or 
collaborators and worked on independently. Once the changes are completed they can be merged back into common branches.  We will look at issues 
more in our fourth lab.

Click on **Issues -> List** and then click on **New Issue**.  Note the fields here and available options then click **Cancel**.

### Merge requests
Merge request are where we can find all the details for any open merge request, merged, or closed request.  Since we 
finished our previous merge request there are no open ones.  Click on the ```Merged``` heading at the top of the window 
to see our completed merge.  Click on the **Update README.md** merge.  Notice it has the same details that it did 
when we submitted it: source branch, destination branch, file(s) updated, changes made, approvals, and users who executed the change.
 
### CI/CD 
This is where we will run our CI/CD pipelines from, edit our pipeline files, and view jobs.  Pipelines are the engine that 
drives CI/CD.  Currently we don't have any but we will create one in the next step. 

### Settings -> CI/CD -> Runners
The last thing we will look at is under **Settings -> CI/CD**.  Click the Expand button by Runners to see runners 
installed.  You should see many runners under 'Shared runners'.  The runners are configured 
as 'shell executors' which means they operate like a Linux BASH prompt when used since they were configured on a Linux system.
 Any commands sent to them from the script must be able to be executed from the host that gitlab-runner is running on, meaning 
 that any packages or dependencies must be installed on the OS that the runner is using.  There are inherent limitations 
 with shell executors that we will explore in a further lab. 

## Step 4 - Create your first CI/CD pipeline
Click back on the Repository link in your toolbar which takes you back to the files screen.  Click the ```+↓``` button 
and choose 'New File'. This will take you to a Web IDE editor.  The name of the file should be '.gitlab-ci.yml' and the 
following contents should be pasted into the file. The file name has to be exact for the pipeline to be picked up.

The pipeline has three stages: build, test, and deploy. Stage names are arbitrary as are tasks within those stages.  
Build stage has one task called build-job.  Test stage has three tasks: test-job1, test-job2, and test-job3.  Deploy has one final 
stage, deploy-prod.  These names help us visualize the function being performed when we review the pipeline. Be as 
descriptive as possible.    

Make sure you copy the data from below using the **Copy** icon on the right side of the text box.

**.gitlab-ci.yml**
```yaml
---
stages:
  - build
  - test
  - deploy

build-job:
  stage: build
  script:
    - echo "Hello, $GITLAB_USER_LOGIN!"

test-job1:
  stage: test
  script:
    - echo "This runner is using the user account '"$USER"'."
    - echo "Who is logged in using the command 'whoami'"
    - whoami

test-job2:
  stage: test
  script:
    - echo "Different commands can be called on multiple lines."
    - pwd
    - ls -al
    - ps -aux | grep gitlab-runner

test-job3:
  stage: test
  script:
    - echo "This job tests something, but takes more time than test-job1 and 2."
    - echo "After the echo commands complete, it runs sleep for 20 seconds"
    - echo "which simulates a test that runs 20 seconds longer than test-job1 and 2"
    - sleep 20

deploy-prod:
  stage: deploy
  script:
    - echo "This job deploys something in the $CI_COMMIT_BRANCH branch."
  environment: production
```  

Note that all the actions in this script are typical actions you could perform on a BASH prompt.  Whoami, echo, pwd, ls, 
ps, grep and sleep were all used in this example, but any application that was installed on the gitlab-runner shell can 
be called from within the .gitlab-ci.yml file.    
 
***Commit changes to .gitlab-ci.yml***
 
Change commit message to 'Add new CI pipeline file.' and choose the blue button **Commit changes**.

[Back to Top](#table-of-contents)

## Step 5 - Review CI/CD pipeline
This file is the trigger to kick off a CI/CD pipeline. Pipelines sometimes take a minute or more to be picked up.  This is 
why the lab has so many shell runners configured to help pick up more jobs quicker but it still make take 1-2 minutes.

***View CI/CD pipelines***

To view the results, click on CI/CD in the toolbar. It takes you to the **Pipeline** section.  
You should see one pipeline in some stage of completion.  This screen also tells you who/what triggered the pipeline, 
time elapsed, stages in the pipeline, and jobs in the stages (hover over the stages). 

![CI/CD Pipelines](../images/Lab01-Pipeline_1.jpg)

***Click Status button for the pipeline***

The status button will say either **Pending**, **Passed**, **Running**, or **Failed**.  If you click 
This will take you to the job details.  Each job within the pipline should also be in one of the following states: Passed, Failed, Running, 
Pending.  The task will stay in pending status until a gitlab-runner picks it up.  Once it is picked up it will move to 
running stage until the job is either passed or failed.  

![CI/CD Jobs](../images/Lab01-Pipeline_2.jpg)

***Click on build-job***

Click on the job named **build-job** which should be in the first stage of the pipeline. This screen shows the terminal 
output of the gitlab-runner.  The output will show you which runner it was executed on, 
the type of executor it is (shell, docker, kubernetes, etc), the server it is running on, the setup, execution of steps, 
and teardown of that individual job.  Look at each individual job to find out what the output of the job is.   You can 
click the back button in your browser (or mouse) from the jobs screen to go back to the pipeline.

![CI/CD Job Details](../images/Lab01-Pipeline_3.jpg)

Questions: 
- Did all the jobs succeed? 
- Do all the jobs use the same runner?
- Was the executor of each job the same?
- What was the output of each job?

[Back to Top](#table-of-contents)

## Step 6 - Create new stage to test for YAML formatting

Let's create a new file in our repository and add it the CI/CD pipeline.  We are going to use a BASH command called 
```yamllint``` to validate if a YAML file is formatted properly.

***Create new YAML file***

Choose **Repository -> Files** from the toolbar.  Click the ```+↓``` button and choose 'New file'.  
Copy the contents (using the copy button) below and paste into the editor.  The filename should be test.yml.  

**test.yml**
```yaml
var1: Test1
var2= Test2
```

Commit the changes to the main branch using the **Commit** button.

**NOTE**: Notice if you go back to CI/CD a new pipeline was created and should be running or completed.  
This was kicked off because we don't have any conditions on WHEN to kick off the pipeline in our gitlabs-ci.yml file.  
Ignore this for now.  We will come back to the pipeline after the next step.  

***Create linter.sh file***

Now create the bash script that will execute the commands to do the ```yamllint``` command.  Go back to Repository -> 
```+↓``` -> New file.  The filename will be linter.sh and paste the contents below into the editor.

```python
#!/usr/bin/env bash

###############################################################################
# linter.sh
# -----
# looks through the current directory for all files ending in .yml
# and then passes them through yamllint which checks for valid yaml syntax
#
# YamlLint can be found at  https://pypi.python.org/pypi/yamllint
#
# Installed via pip with $ pip install yamllint
#
###############################################################################

for file in $(find . -name "*.yml")
do
    yamllint $file
    rc=$?
    if [ "$rc" -ne 0 ] ; then
        exit $rc
    fi
done

echo "Linting Passed"

exit 0

```

Click **Commit** to commit the changes to the main branch.  Once again the pipeline kicked off but we aren't quite ready 
to view it yet.

***Edit .gitlab-ci.yml file to add the Validate YAML stage***

Go back to Repositories and click on the .gitlab-ci.yml file.  Notice the IDE editor option has been replaced with 
**Edit in pipeline editor** since this is a pipeline file.  Click that button to go to the editor.   We are going to add 
a new stage, validate, as the first job to complete.  Copy the entire section below using the ***copy button*** and paste in 
into the Web IDE.

```yaml
---
stages:
  - validate
  - build
  - test
  - deploy

validate YAML:
  stage: validate
  script:
    - bash linter.sh

build-job:
  stage: build
  script:
    - echo "Hello, $GITLAB_USER_LOGIN!"

test-job1:
  stage: test
  script:
    - echo "This runner is using the user account '"$USER"'."
    - echo "Who is logged in using the command 'whoami'"
    - whoami

test-job2:
  stage: test
  script:
    - echo "Different commands can be called on multiple lines."
    - pwd
    - ls -al
    - ps -aux | grep gitlab-runner

test-job3:
  stage: test
  script:
    - echo "This job tests something, but takes more time than test-job1 and 2."
    - echo "After the echo commands complete, it runs sleep for 20 seconds"
    - echo "which simulates a test that runs 20 seconds longer than test-job1 and 2"
    - sleep 20

deploy-prod:
  stage: deploy
  script:
    - echo "This job deploys something in the $CI_COMMIT_BRANCH branch."
  environment: production
``` 
Click **Commit changes** then choose the MAIN branch and click **Commit** and let's go check the pipeline.

***Validate pipeline***

If you stay on this screen for just a moment, it should update and give visibility into the stages and option to view 
the pipeline.  Click 'View pipeline' when it appears on the commit screen or go to **CI/CD -> Pipeline**. Click on the pipeline for 
'Update .gitlab-ci.yml file'.  Notice the new stage has been added at the front of the pipeline: validate and there is 
one job in the pipeline: validate YAML.  Wait for the job to complete before continuing.

WHOA! Job Failed!  Click on the failed job in the pipeline to view the job output.

***Fix YAML file***

The job failed because yamllint failed to properly validate all the YAML files in the directory and threw an error.  

![CI/CD Job Failed](../images/Lab01-CICD_Failed.jpg)

Notice that we have two errors in ./test.yml at the bottom of the screen.  The first one shows us on line 1, character 1, 
1:1, that we have a missing document start sequence of '---'.  Position 3:1 or line 3, character 1 we have an issue that 
a ':' was expected but could not be found.  Let's correct these issues and re-run our pipeline.  Notice that the first 
one was just a warning, the second one was the error.  Linting would have passed successfully if we had only left off 
the starting sequence characters. 

Go back to the repository by clicking on Repository -> Files.  Click on test.yml, and choose **Open in Web IDE**.  
Copy and paste info below to correct these errors. 

```yaml
---
var1: Test1
var2: Test2
```
Commit **Create commit**, choose **Commit to main branch**, and choose **Commit**. This will kick off another CI/CD 
pipeline process that you can go view.

***Verify CI/CD pipeline executed properly***

Go back to CI/CD -> Pipeline

Choose the status icon for the latest 'Update test.yml' pipeline.  Click on the **validate YAML** job and you should see 
that Linting Passed successfully and so the job exited successfully.

Now that you have a basic understanding of Gitlab, let's move onto more relevant topics of using Gitlab to execute 
gitlab-runners to perform network automation.

Go ahead and move to the next lab, [Lab02-Basic_Network_Automation](../Lab02-Basic_Network_Automation/README.md) to 
start using Ansible to interact with our Cisco IOS device via CI/CD pipeline.

[Back to Top](#table-of-contents)

## TROUBLESHOOTING

If you get stuck, the final configuration is located in LAB01-Introduction_to_Gitlab/final.  

[Back to Top](#table-of-contents)