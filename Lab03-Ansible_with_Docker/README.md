# Lab 03 - Ansible with Docker

In this lab we will use gitlab-runners to execute Docker containers configured with our proper Ansible libraries and 
 other dependencies to run our pipelines.  Since our ansible playbooks rely on newer plugins than are supported in a basic 
 Ansible install we will need to use a Docker container with updated ansible-galaxy collection of cisco.ios for our 
 previous CI file to work.

**Objective**: Understand how to launch Ansible from gitlab-runners as a Docker executor against a network environment.  
You will create your own local gitlab-runner using a predefined Docker image and register it for your project.  Once you have
the runner installed, we will re-run the playbook to validate that it works as expected.

## Table of Contents

[[_TOC_]]

## How Docker containers work in Gitlab-runner CI
Gitlab-runner can run docker containers that can either execute jobs directly from their shell or launch docker containers 
within them that have custom configuration.  You can build each instance the runner that gets executed by specifying the runtime 
commands that are necessary.  For this example we will be using a widely available public image, alpine:3.17.2 as our base 
Docker image for the gitlab-runner.

There are many reasons to launch unique docker images but a few are to reduce build time at execution of the job and ensure specific versions of 
images are used.  In our case it takes a long time to install Ansible and other dependencies.  Rather than launch alpine:13.17.2 
, then install Ansible, then update ansible-galaxy collections, etc, you can just use a custom Docker image that was created with all 
the pre-requisites included.  This will reduce our build time and ensure that we have a known good instance of an image with 
all dependencies pre-installed for our CI package execution.  Jobs can either use the base docker image and install necessary packages or 
specify a specific docker image to use and that image will be pulled down to kick off the job.  You will see later in an example.

FUN FACT: You could run gitlab-runner from a docker container that then runs a docker executor that can run other docker 
inside it.  It's like Inception but with Docker! 

![Inception](../images/Lab03-Inception.jpg){width=250px}

[Back to Top](#table-of-contents)

## Review Dockerfile

The docker image you will use has already been created and stored on Docker hub for public access.  
Docker containers are built using a input file called Dockerfile.  
This file contains all the necessary information on what base image to grab (Ubuntu, Centos, Redhat, etc), environment variables, 
setup steps, and cleanup steps to reduce the size of the Docker image. Below are the commands that were used to build 
the Docker image.   

```
# Start with base image from ubuntu:22.04
FROM ubuntu:jammy

# Disable Prompt During Package Installation
ARG DEBIAN_FRONTEND=noninteractive
LABEL version="0.1"
LABEL description="Custom Docker Image for Ansible and python requests"

# Update software repository
RUN apt-get update

# Install applications
RUN apt-get install -y ansible python3-pip
RUN pip3 install yamllint paramiko ansible-pylibssh

# Update Ansible Galaxy collections
RUN ansible-galaxy collection install cisco.ios:==4.3.1
RUN rm -rf /var/lib/apt/lists/*
RUN apt-get clean

# Set Ansible Variables
ENV ANSIBLE_HOST_KEY_CHECKING=False
ENV ANSIBLE_DEPRECATION_WARNINGS=False

```

Notice that the first line indicates the base image we are using.  We will be using Ubuntu:Jammy which is the codename 
for 22.04 (Long Term Release).  The next line disables prompting during the package installation so that the installation 
doesn't get stuck.  The next few lines are generic labels.  We use apt-get to download packages so they are not interactively 
prompting when installing.  First we update the repository.  Then we add Ansible and python3-pip. Finally we use pip to install 
other dependencies including yamllint, paramiko, and ansible-pylibssh.  Note: These were all installed manually on the 
gitlab-runner shell that we have been using up until now.  

The next line is the reason we had to create this docker container.  It forces Ansible-Galaxy to download an updated collection 
for the Cisco.ios library to version 4.3.1 which is the current version.  The final run commands clean up our installation 
directories in order to reduce the image size.  Finally, we set some Ansible Variables that we know we will want in our lab.

[Back to Top](#table-of-contents)

## Create new gitlab-runner

Each student will create a custom gitlab-runner on your POD Virtual Machine.  You will register it to your own project and 
validate that it is communicating before re-running the last playbook.  Docker and gitlab-runner are already installed on your VM and the Docker 
image described above has already been hosted on Dockerhub.  You will not need to interact with Docker directly, as this 
will be handled by gitlab-runner once you register it.  

* SSH into your POD VM

Using Putty or SecureCRT, SSH into the VM assigned to you. See your lab worksheet for credentials.  

* Verify gitlab-runner is started

Once logged into the CLI let's verify if gitlab-runner is started.  

``` gitlab-runner status ```

Notice we get a fatal error.  'The --user is not supported for non-root users'. This message indicates that the user has 
to have root access in order to run some gitlab-runner commands.  This is due to the way gitlab-runner was installed 
(per gitlab instructions).  Gitlab-runner service start was launched using the sudo command because it requires root access, 
however the configuration for runners exists under a service account for 'gitlab-runner'.  We must run all of our 
gitlab-runner configuration commands with the service using the sudo command to elevate the privileges.  Let's re-run the command with sudo access.

``` sudo gitlab-runner status ```

You will be prompted for sudo password, which is 'ubuntu'.  Once you enter this you should see ```gitlab-runner: Service is running```.  
If you see this message, you are ready to register your docker runner. 

Note:  If you run the command: ```ps -aux | grep gitlab-runner ```, you can see the service command that specifies the runner, 
who is running it, configuration files being used, etc. 

[Back to Top](#table-of-contents)

## Register docker runner

Let's go ahead and register the runner using the docker image Alpine:3.17.2 as our base executor.    

### Get registration token

In order to register the runner we will first need a unique token provided by the gitlab project, group, or system. In this example we
 will use the project settings to register the gitlab-runner.  In order to get the token, from your project repository, go to 
 Settings -> CI/CD -> Runners and expand the area.  
 
 Notice there are three different sections to the runners, project runners, shared runners, and group runners. Project runners are associated only 
 with your project.  Group runners can be shared between projects.  If you are running your own server you can also configure shared runners, 
 which we are using for shell executors. If you were running this from Gitlab.com, shared runners would shared with the entire gitlab 
 population which makes for an exceptionally powerful distributed system (although possibly compromised by security).  
 
 NOTE: We do not recommend using public shared runners for any production or secure info work (IP addresses, passwords, etc).
 
Again project runner is what we will define.  Notice that your Gitlab server has some details for you to follow when setting 
up the project runner.  The page provides you with the URL you need as well as the registration token.  Each registration token 
is unique but runner URL should all be the same: http://e16ubuntu1gitlab-ce.datalinklabs.local/.  Copy your registration token using the 
clipboard icon to the right of it.

![Gitlab Runner Registration Token](../images/Lab03-Runner_Token.jpg)

### Register runner with docker image

You can manually register the runner with the command ```sudo gitlab-runner register``` and follow the prompts.  It is a 
bit cumbersome to follow however and mistakes are easy to make and hard to correct due to the nature of CLI.  Instead of manually registering 
you will use the following script to paste into the gitlab-runner VM assigned to your POD.  Be sure to update your registration 
token to the one provided in the previous step. Copy the following section into a text editor and update the <<INSERT_TOKEN_HERE>> 
string before pasting into the gitlab-runner VM. 

```
sudo gitlab-runner register \
  --non-interactive \
  --url "http://e16ubuntu1gitlab-ce.datalinklabs.local/" \
  --registration-token "<<INSERT_TOKEN_HERE>>" \
  --executor "docker" \
  --docker-image alpine:3.17.2 \
  --description "docker-alpine" \
  --maintenance-note "Base Docker image using Alpine 3.17.2" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```

[Back to Top](#table-of-contents)

### Verify runner is associated

Once the new registration command is entered, two things happen.  1) An update occurs to a file in /etc/gitlab-runner/config.toml and 
2) The runner sends a message to the gitlab server.  To see if the registration was successful, refresh your gitlab webpage. 

![Gitlab Runner Registration Status](../images/Lab03-Runner_registered.jpg)

Notice that the project runner is now listed with an ! status.  We should see the description of the runner as **docker-runner**.

If you hover over the ! you will see the error listed as "Runner has never contacted this instance.".  
Let's verify gitlab-runner configuration. 

[Back to Top](#table-of-contents)

### Verify gitlab-runner configuration

Verify the gitlab-runner is configured with the following command.

```
sudo gitlab-runner list
```

You should see a single executor listed as docker, with the token specified, and the URL of the gitlab server.  If you don't see this, redo the previous command.

The output also shows us where the config file is located at.  We can verify it by using the **cat** command but since it 
is in the /etc/ directory we will need elevated privileges.

```
sudo cat /etc/gitlab-runner/config.toml
```

You should see similar info to the previous step but this is the complete configuration.  If everything looks right let's check
 one more thing.
 
[Back to Top](#table-of-contents)
 
### Verify runner status

You can verify the status of all of the registrations using the following command.  The command causes the runner to 'ping' all registrations 
and verify if they are active.

```
sudo gitlab-runner verify
```

The output should say the following with your unique runner ID. 

```
Verifying runner... is alive                        runner=JETqtY8D
```

If it is alive, go back to Gitlabs and refresh your Settings -> CI/CD -> Runners page again.  

Notice the runner now shows green status.  If you click on the link you can see details about the runner including the 'last contact' 
time.  In the essence of saving time on this lab: Although the runner has contacted gitlab and verified the registration, it will 
not continue to communicate with the server unless we restart the gitlab services.  The runners must continually poll the gitlab server to 
check for available jobs based on their assignment (executor) as well as tags associated with them.

[Back to Top](#table-of-contents)

### Restart gitlab-runner services

Go back to the gitlab-runner VM and type the following command to restart the services.

``` 
sudo gitlab-runner restart
```

We are now ready to execute our CI pipeline using the docker executor.

[Back to Top](#table-of-contents)

## Update .gitlab-ci.yml CI file

Go to your Gitlab repository and find the file ```Lab03-Ansible_with_Docker/.gitlab-ci.yml```.  Open using the Web IDE.

The file contains a basic task that is just a placeholder.  Let's delete everything and replace with the following commands.

```yaml
---
stages:
  - docker_test

generic docker:
  stage: docker_test
  script:
    - echo "This should be our base docker instance of alpine:3.17.2" 
    - hostname
    - echo "Verify OS Release is Alpine 3.17.2." 
    - cat /etc/os-release

specific docker:
  stage: docker_test
  image: alpine:3.16.4
  script:
    - echo "This instance of docker is running inside our base docker instance of alpine:3.17.2. (Docker in docker)"
    - echo "This public specific version of docker should be alpine:3.16.4."
    - hostname
    - echo "Verify OS Release is Alpine 3.16.4."    
    - cat /etc/os-release

custom docker:
  stage: docker_test
  image: tcurtis45/ansible-basic:latest
  script:
    - echo "This is our custom docker running tcurtis45/ansible-basic:latest."  
    - echo "We should be able to run Ansible and ansible-galaxy commands now."
    - hostname
    - echo "Verify OS Release is Ubuntu 22.04."
    - cat /etc/os-release
    - echo "Check to see if the ansible command is found.  If so the path will be shown."    
    - command -v ansible
    - ansible-galaxy collection list
```

[Back to Top](#table-of-contents)

**Commit** the file to the main branch.

## Verify Pipeline
Go back to CI/CD and open the pipeline.  If the proper runner picked up the jobs, the tests should have passed. If the jobs failed it may be due to the 
runner that was selected for the job.  If so, refresh the job until it is picked up by the docker-alpine executor.
  
Open each of the test and look at which runner picked up the jobs.  If you see a job with the docker as the executor, notice that 
the process first pulls down the docker image, then loads the image, fetches the repository on the image, runs the script, 
and then exits (and tears down the docker instance).

Notice that you did not have to download the docker image, just specify a publicly available one in your CI file.

If you did not see the docker executor for one of the jobs (or if all you see are docker executor jobs), refresh one of the jobs.  
From the pipeline, open the job, and then click the refresh icon in the top right ![Gitlab Job Refresh](../images/Lab03-Refresh.jpg){width=25 height=25px}.
Eventually you will hit one of the shell executors.  Notice that the script does not include the docker image information when run in shell.

[Back to Top](#table-of-contents)

## Analyse Results

Why didn't our job use the docker instance every time?  Since we specified the image for two stages, shouldn't the docker executor always 
pick up the job with the image specified?

Unfortunately this is a nuance to how the executors works.  Even per documentation, shell won't pick up the 'image' keyword but will still 
execute the script.  This is by design but easy to overcome.  You need to use tags to specify which executor runs our job to ensure they are 
picked up by the proper runners.  Multiple runners can be setup with the same tags for load sharing/high availability.

NOTE: Don't get me started on how confusing this was to figure out :)

[Back to Top](#table-of-contents)

## Use tags to run specific images in the Docker executor

First we will need to add a tag to our runner.  We could have done this during the initial registration but we can also do it 
via the web interface.  Find your **Assigned project runners** and click the pencil icon beside the link.  The last field 
is called **Tags**.  Go ahead and put the tag ```docker``` in this field and click **Save changes**.

Note: This runner can also pick up 'untagged' jobs so it is possible it will pick up our first job which doesn't specify an image.

Go to your project -> Settings -> CI/CD -> Runners and click **Expand**.  
Go back to the Repository -> Lab03-Ansible_with_Docker -> .gitlab-ci.yml file and **Open in Web IDE**.  Replace the contents of the 
file with what is listed below, and commit the file to the main branch.

```yaml
---
stages:
  - docker_test

untagged job:
  stage: docker_test
  script:
    - echo "This is the untagged job and could execute on shell or docker depending on which one picked it up." 
    - hostname
    - echo "OS will either be Alpine 3.17.2 for Docker or Ubuntu 22.04 for the shell executor" 
    - cat /etc/os-release

generic docker:
  stage: docker_test
  script:
    - echo "This should be our base docker instance of alpine:3.17.2" 
    - hostname
    - echo "Verify OS Release is Alpine 3.17.2." 
    - cat /etc/os-release
  tags:
    - docker

specific docker:
  stage: docker_test
  image: alpine:3.16.4
  script:
    - echo "This instance of docker is running inside our base docker instance of alpine:3.17.2. (Docker in docker)"
    - echo "This public specific version of docker should be alpine:3.16.4."
    - hostname
    - echo "Verify OS Release is Alpine 3.16.4."    
    - cat /etc/os-release
  tags:
    - docker

custom docker:
  stage: docker_test
  image: tcurtis45/ansible-basic:latest
  script:
    - echo "This is our custom docker running tcurtis45/ansible-basic:latest."  
    - echo "We should be able to run Ansible and ansible-galaxy commands now."
    - hostname
    - echo "Verify OS Release is Ubuntu 22.04."
    - cat /etc/os-release
    - echo "Check to see if the ansible command is found.  If so the path will be shown."    
    - command -v ansible
    - ansible-galaxy collection list
  tags:
    - docker
```

[Back to Top](#table-of-contents)

## Verify pipeline

Go to your CI/CD pipeline for the job that just kicked off.  You should see all jobs have run successfully.  The first test, 
may have run in the shell or the docker container.  The rest of the jobs will only have run in the base docker container.

Notice for the stage **custom docker** that the command ```ansible-galaxy collection list``` is used.  This will print out the 
list of ansible-galaxy collections installed so we can verify that cisco.ios version 4.3.1 is installed.   

**Best practice is to use tags to specify which runners should be executing your jobs.**  

If you refresh the first job enough times you should see the shell executor pick it up.  Notice the difference in log 
messaging, hostname, and OS Release.  

[Back to Top](#table-of-contents)

## Update CI Pipeline to use new snmp.yml file

Now that we understand how to use different docker images, let's use our custom image with Ansible to apply the playbook 
that failed from the previous lab.  From the repository -> Lab03-Ansible_with_Docker -> .gitlab-ci.yml file select **Open in Web IDE**.

Delete everything in the CI file and replace with the following snippet.  

```yaml
---
stages:
    - validate
    - deploy

image: tcurtis45/ansible-basic:latest

validate YAML:
    stage: validate
    script:
        - cd Lab03-Ansible_with_Docker
        - bash linter.sh
    tags:
        - docker

Deploy SNMP:
    stage: deploy
    script:
        - ansible-playbook -i inventory.yml Lab03-Ansible_with_Docker/snmp.yml
    tags:
        - docker
```  

Note that we specified a global image to use for all stages of this script and added the tag ```docker``` to each of our jobs.

**Commit** the file to the main branch and let's view the pipeline output.

[Back to Top](#table-of-contents)

## Verify pipeline status
Go back to CI/CD -> Pipelines and look at the recent pipeline that kicked off.  You should see two successful jobs for ```validate YAML``` and
 ```Deploy SNMP```.  Drill down into the jobs to verify they were kicked off with the proper executor.  

![Lab03 - SNMP Success](../images/Lab03-SNMP_success.jpg)

You see from the image above that the gitlab-runner selected was the docker-alpine using the docker executor.  The executor 
pulled the tcurtis45/ansible-basic:latest image to use as the environment and then executed Ansible.  We know that Deploy SNMP would 
 fail if it were not because ansible is not included on the base docker images. If this task is successful then we definitely 
 are using the right docker container.
 
That's it for this lab.  Let's head into the next lab for Issues and Merge processes to provision new services on the POD routers.

[Back to Top](#table-of-contents)

## Conclusions

Docker containers give infinite possibilities of build environments to be used within our CI pipeline.  By using the docker executors 
and either installing apps on base images or building custom docker files, we are able to ensure consistency in environments, 
help version control, reduce build times, and reduce the number of gitlab-runner registrations required by specifying images in our jobs.

This is a very powerful tool in building and testing software but even in network automation there is value to not having dependencies 
on the gitlab-runner server itself and instead building your own docker files.  You may not be the administrator of the gitlab-runner server 
in your environment and be forced to use containers to accomplish what you need.  

Move ahead to the next lab, [Lab04 - Proper Issues and Merge Processes](../Lab04-Proper_Issues_and_Merge_Processes/README.md).
